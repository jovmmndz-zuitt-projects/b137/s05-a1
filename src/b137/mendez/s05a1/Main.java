package b137.mendez.s05a1;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        ArrayList<String> numberContact1 = new ArrayList<String>();
        numberContact1.add("+639152468596");
        numberContact1.add("+639228547963");
        contact1.setNumbers(numberContact1);
        ArrayList<String> addressContact1 = new ArrayList<String>();
        addressContact1.add("my home in Quezon City");
        addressContact1.add("my office in Makati City");
        contact1.setNumbers(addressContact1);

        Contact contact2 = new Contact();
        contact2.setName("Jane Doe");
        ArrayList<String> numberContact2 = new ArrayList<String>();
        numberContact2.add("+639162148573");
        numberContact2.add("+639173698541");
        contact1.setNumbers(numberContact2);
        ArrayList<String> addressContact2 = new ArrayList<String>();
        addressContact2.add("my home in Caloocan City");
        addressContact2.add("my office in Pasay City");
        contact1.setNumbers(addressContact2);


/*

        Activity (s04-a1 & s05-a1 submissions)
Following the established steps in creating a new Java project from the previous activities, have the students create a
project named "s05-a1" under the base package "b137.<lastname>.s05a1".
  Have the students create the following classes in the package "b137.<lastname>.s05a1":
Main.java
Contact.java
Phonebook.java

Declare the following instance variables for the Contact class:
name of data type String
numbers, an ArrayList of data type String
addresses, also an ArrayList of data type String

Both numbers and addresses are to be initialized as empty ArrayLists that can only take in String values.

Define a default (empty) constructor and a parameterized constructor for the Contact class. The parameterized
constructor will take in String data for the number and address that will then be added to their respective ArrayLists.
Define the getter and setter methods for the instance variables of the Contact class.
In the Phonebook class, create a single instance variable:
an ArrayList of Contact objects named contacts. This is to be initialized as an empty ArrayList that can only take in
Contact objects.
Define a default constructor and a parameterized constructor for the Phonebook class. The parameterized constructor
will take in a Contact object that will then be added to the contacts ArrayList.
Define getter and setter methods for the Phonebook class.
In the main method of the Main class, instantiate a new object named phonebook from the Phonebook class.
Instantiate 2 contacts from the Contact class, give each contact multiple numbers and addresses using the class's
setter methods.
Add both contacts to the phonebook object using its setter method.
Define a control structure that will output a notification message in the console if the phonebook is empty,
and will print all the information of all contacts found in the phonebook otherwise.
Expected console output:


*/
    }
}
