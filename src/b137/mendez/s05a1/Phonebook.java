package b137.mendez.s05a1;

import java.util.ArrayList;

public class Phonebook {
    // Properties
    private ArrayList<String> contacts = new ArrayList<String>();;

    // Empty Constructor
    public Phonebook() {}

    // Parameterized Constructor
    public Phonebook(String contacts) {
        this.contacts.add(contacts);
    }

    // Getters
    public ArrayList<String> getContacts() {
        return contacts;
    }

    // Setters
    public void setContacts(ArrayList<String> newContacts) {
        this.contacts = newContacts;
    }

    // Methods

}
