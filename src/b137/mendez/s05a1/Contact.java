package b137.mendez.s05a1;

import java.util.ArrayList;

public class Contact {
    // Properties
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();;
    private ArrayList<String> addresses = new ArrayList<String>();

    // Empty Constructor
    public Contact() {}

    // Parameterized Constructor
    public Contact(String number, String address) {
        this.numbers.add(number);
        this.addresses.add(address);
    }

    // Getters
    public String getName() {
        return name;
    }
    public ArrayList<String> getNumbers() {
        return numbers;
    }
    public ArrayList<String> getAddresses() {
        return addresses;
    }

    // Setters
    public void setName(String newName) {
        this.name = newName;
    }
    public void setNumbers(ArrayList<String> newNumbers) {
        this.numbers = newNumbers;
    }
    public void setAddresses(ArrayList<String> newAddresses) {
        this.addresses = newAddresses;
    }

    // Methods

}
